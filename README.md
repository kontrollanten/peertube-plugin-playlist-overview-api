# REST API for PeerTube video playlist overview

Adds a REST endpoint (/plugins/playlist-overview-api/router/:channelName) which returns all
the channels playlists and it's 10 latest videos.
