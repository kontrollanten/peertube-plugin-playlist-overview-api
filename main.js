async function register ({
  getRouter,
  peertubeHelpers,
  registerHook,
  registerSetting,
  settingsManager,
  storageManager,
  videoCategoryManager,
  videoLicenceManager,
  videoLanguageManager
}) {
  const { config, database, logger, videos } = peertubeHelpers
  const router = getRouter()
  router.get('/:channelName', async function (req, res) {
    try {
      const videoLimit = 10
      const [rawPlaylists] = await database.query(`
        SELECT
          "videoPlaylistElement"."videoId",
          "videoPlaylist".id,
          "videoPlaylist".uuid,
          "videoPlaylist".name,
          "videoPlaylist".description
        FROM "videoPlaylist"
        INNER JOIN "videoChannel" ON "videoChannel"."id" = "videoPlaylist"."videoChannelId"
        INNER JOIN actor ON actor.id = "videoChannel"."actorId"
        LEFT JOIN "videoPlaylistElement" ON "videoPlaylist".id = "videoPlaylistElement"."videoPlaylistId" AND "videoPlaylist".privacy = 1
        LEFT JOIN video ON video.id = "videoPlaylistElement"."videoId"
        WHERE actor."preferredUsername" = ?
        ORDER BY CASE
          WHEN "video"."originallyPublishedAt" IS NOT NULL THEN "video"."originallyPublishedAt"
          ELSE "video"."publishedAt"
        END
        DESC
      `, { replacements: [req.params.channelName] });
      const playlists = {}
      const startedLoadingCount = {};

      await Promise.all(rawPlaylists.map(async (row) => {
        playlists[row.id] = playlists[row.id] || {
          description: row.description,
          id: row.id,
          name: row.name,
          videos: [],
          uuid: row.uuid,
        }
        startedLoadingCount[row.id] = startedLoadingCount[row.id] || 0;
        if (!row.videoId || startedLoadingCount[row.id] >= videoLimit) return

        startedLoadingCount[row.id]++;
        const video = await videos.loadByIdOrUUID(row.videoId)

        try {
          const preview = video.getPreview()
          const url = preview.getFileUrl ? preview.getFileUrl(video) : preview.getOriginFileUrl(video) // getOriginFileUrl since PeerTube 5.2.0

          video.dataValues.previewPath = url.replace(config.getWebserverUrl(), '')
        } catch (error) {
          logger.error(`Failed to add previewPath for video ${video.id}`)
          logger.error({ error })
        }

        if (video.privacy !== 1) {
          logger.debug(`Video ${video.uuid} is not public, not adding.`)
          return
        }

        playlists[row.id].videos.push(video)
      }));

      res.json(Object.values(playlists))
    } catch (error) {
      logger.error(error)
      res.status(500).end()
    }
  })
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
